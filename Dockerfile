FROM python:3.11-alpine

ADD src/*.py /src/
ADD config/*.toml /config/
ADD config/.token.example /config/.token.example
ADD requirements.txt /

RUN pip install -r /requirements.txt

CMD ["python", "/src/__main__.py"]

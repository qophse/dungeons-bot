import dnd
import util
import random
from creature import Creature
import lang

class Monster(Creature):
    """
    Describes a Monster, a hostile Creature.
    """

    def __init__(self, monster) -> None:

        super().__init__(monster["name"], dnd.roll_sum(monster["hit_points_roll"]))
        self._monster = monster

        self._xp = self._monster["xp"]
        self._challenge_rating = self._monster["challenge_rating"]
        self._image_url = dnd.get_monster_image_link(self._monster)

        abilities = dnd.get_abilities()
        for a in abilities:
            ability = dnd.get_ability(a["index"])
            self._abilities[a["index"]] = self._monster[ability["full_name"].lower()]

        for p in self._monster["proficiencies"]:
            proficiency = self.add_proficiency(p["proficiency"]["index"])
            if (proficiency["type"].lower() == "skills"):
                self._skills[proficiency["reference"]["index"]] = p["value"]

            elif (proficiency["type"].lower() == "saving throws"):
                self._saving_throws[proficiency["reference"]["index"]] = p["value"]

        self._proficiency_bonus = dnd.cr_proficiency(self._challenge_rating)


    def armor_class(self, armor: dict = None) -> int:

        return self._monster["armor_class"]


    def emoji(self) -> str:

        if (self._monster["type"] in util.emojis["monster_types"]):
            return util.emojis["monster_types"][self._monster["type"]]

        return ""


    def short_bio(self) -> str:

        return lang.bio_monster.short.choose(
            "{} {}".format(self.emoji(), self._name),
            "{} {}".format(util.emojis["health"]["full"], self._hitpoints),
            "{}{}".format(util.emojis["equipment"]["challenge_rating"], self._challenge_rating),
            "{} {}".format(util.emojis["experience"]["xp"], self._xp)
        )


    def bio(self) -> str:

        return lang.bio_monster.long.choose(
            "{} {}".format(self.emoji(), self._name),
            "{}".format(self.hitpoint_bar()),
            "{} {}".format(util.emojis["equipment"]["armor"], self.armor_class()),
            "{}{}".format(util.emojis["equipment"]["challenge_rating"], self._challenge_rating),
            "{} {}".format(util.emojis["experience"]["xp"], self._xp)
        )


    def attack(self, other: Creature) -> dnd.AttackResult:

        bonus = 0
        damage_dice = []

        if ("actions" not in self._monster or not self._monster["actions"]):
            return dnd.AttackResult(0, dnd.AttackType.MISS, "")

        action = random.choice(self._monster["actions"])

        # TODO: rechoose an action if its not a damaging action
        if ("damage" in action):
            for damage in action["damage"]:
                if ("choose" in damage):
                    for _ in range(damage["choose"]):
                        damage_dice.append(random.choice(damage["from"]["options"])["damage_dice"])

                else:
                    damage_dice.append(damage["damage_dice"])

            if ("attack_bonus" in action):
                bonus = action["attack_bonus"]

        hit_check = dnd.roll_sum("1d20")
        util.debug(f"Monster Attack roll: {hit_check} + {bonus}")
        hit = 0

        # Critical success
        if (hit_check == 20):
            for die in damage_dice:
                hit += dnd.roll_sum(die, die)
            return dnd.AttackResult(max(1, hit), dnd.AttackType.CRIT_SUCCESS, action["name"])

        # Critical failure
        elif (hit_check == 1):
            return dnd.AttackResult(0, dnd.AttackType.CRIT_FAILURE, action["name"])

        # Normal attack
        elif (hit_check + bonus >= other.armor_class()):
            for die in damage_dice:
                hit += dnd.roll_sum(die)
            return dnd.AttackResult(max(1, hit), dnd.AttackType.NORMAL, action["name"])

        return dnd.AttackResult(0, dnd.AttackType.MISS, action["name"])

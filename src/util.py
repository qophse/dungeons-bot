import toml
import logging
import os
import shutil

def debug(s: str):

    logger.debug(s)


def error(s: str):

    logger.error(s)


def warn(s: str):

    logger.warning(s)


def info(s: str):

    logger.info(s)


def log(s: str):

    logger.log(0, s)


SECONDS_PER_DAY = 86400
SECONDS_PER_HOUR = 3600
SECONDS_PER_MINUTE = 60

def format_duration(seconds: int) -> str:
    """Formats the given time in seconds as Days, HH:MM"""

    seconds = int(seconds)
    days, seconds = divmod(seconds, SECONDS_PER_DAY)
    hours, seconds = divmod(seconds, SECONDS_PER_HOUR)
    minutes, seconds = divmod(seconds, SECONDS_PER_MINUTE)

    if (days == 1):
        return "{} day, {:02d}:{:02d}".format(days, hours, minutes)
    elif (days > 0):
        return "{} days, {:02d}:{:02d}".format(days, hours, minutes)

    return "{:02d}:{:02d}".format(hours, minutes)


def get_clock(seconds: int) -> str:

    seconds = int(seconds)
    _, seconds = divmod(seconds, SECONDS_PER_DAY)
    hours, seconds = divmod(seconds, SECONDS_PER_HOUR)
    minutes, seconds = divmod(seconds, SECONDS_PER_MINUTE)

    hours = hours % 12 or 12
    base = 0x1f550 # 🕐

    if (minutes >= 30):
        base = 0x1f55c # 🕜

    return f"{chr(base + hours - 1)}"


def try_load(file_name: str):
    data = {}
    # load defaults
    with open(os.path.join(root, f"config/{file_name}"), "r") as file:
        data = toml.load(file)

    # load existing
    try:
        with open(os.path.join(root, f"data/{file_name}"), "r") as file:
            temp = toml.load(file)

            def set_values(a: dict, b: dict):
                for key in a:
                    if (key in b):
                        if (isinstance(a[key], dict)):
                            set_values(a[key], b[key])
                        elif (not key.startswith("_")):
                            a[key] = b[key]

            set_values(data, temp)

    # or, create defaults
    except FileNotFoundError:
        pass

    with open(os.path.join(root, f"data/{file_name}"), "w") as file:
        toml.dump(data, file)

    return data


logger = logging.getLogger("dungeons")
logger.setLevel(logging.DEBUG)
logging.basicConfig(format = "%(asctime)s %(levelname)-8s %(message)s")

root = os.path.dirname(os.path.dirname(__file__))

# Ensure data dir exists
if (not os.path.exists(os.path.join(root, "data/"))):
    os.makedirs(os.path.join(root, "data/"))

# Init data
emojis = try_load("emojis.toml")
config = try_load("config.toml")

shutil.copyfile(os.path.join(root, "config/.token.example"), os.path.join(root, "data/.token.example"))

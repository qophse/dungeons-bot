import pytest
from src.creature import Creature
from src import dnd

# Amount of times to perform random tasks
N = 100

class CreatureChild(Creature):

    def armor_class(self, armor: dict = {}) -> int:
        return super().armor_class(armor)


    def attack(self, other: "Creature") -> dnd.AttackResult:
        return super().attack(other)


    def emoji(self) -> str:
        return super().emoji()


    def bio(self) -> str:
        return super().bio()


    def short_bio(self) -> str:
        return super().short_bio()


@pytest.fixture
def creature():

    return CreatureChild(
        "Aboleth",
        10
    )


def test_init(creature: CreatureChild) -> None:

    assert creature._name == "Aboleth"
    assert creature._max_hitpoints == 10
    assert creature._hitpoints == 10

    assert creature._proficiency_bonus == 2

    assert len(creature._abilities) > 0
    for a in creature._abilities:
        assert creature._abilities[a] == 0

    assert len(creature._saving_throws) > 0
    for s in creature._saving_throws:
        assert creature._saving_throws[s] == 0

    assert len(creature._skills) > 0
    for s in creature._skills:
        assert creature._skills[s] == 0


def test_instances(creature: CreatureChild) -> None:

    new_creature = CreatureChild("Test", 10)

    assert new_creature is not creature
    assert new_creature._abilities is not creature._abilities


def test_set_max_hitpoints(creature: CreatureChild) -> None:

    assert creature.set_max_hitpoints(20) == 20
    assert creature._max_hitpoints == 20

    assert creature.set_max_hitpoints(0) == 1
    assert creature._max_hitpoints == 1


def test_set_hitpoints(creature: CreatureChild) -> None:

    assert creature.set_hitpoints(5) == -5
    assert creature._hitpoints == 5
    assert creature.set_hitpoints(10) == 5
    assert creature._hitpoints == 10

    assert creature.set_hitpoints(11) == 0
    assert creature._hitpoints == 10


def test_heal(creature: CreatureChild) -> None:

    creature.set_hitpoints(5)
    assert creature.heal(5) == 5
    assert creature._hitpoints == 10
    assert creature.heal(5) == 0
    assert creature._hitpoints == 10


def test_damage(creature: CreatureChild) -> None:

    assert not creature.damage(5)
    assert creature._hitpoints == 5
    assert creature.damage(5)
    assert creature._hitpoints == 0


def test_add_proficiency(creature: CreatureChild) -> None:

    proficiency = creature.add_proficiency("skill-acrobatics")
    assert len(proficiency) > 0
    assert creature._skills["acrobatics"] == 1
    assert proficiency in creature._proficiencies

    proficiency = creature.add_proficiency("saving-throw-cha")
    assert len(proficiency) > 0
    assert creature._saving_throws["cha"] == 1
    assert proficiency in creature._proficiencies

    proficiency = creature.add_proficiency("alchemists-supplies")
    assert len(proficiency) > 0
    assert proficiency in creature._proficiencies


def test_ability_modifier(creature: CreatureChild) -> None:

    creature._abilities["cha"] = 10
    assert creature.ability_modifier("cha") == 0

    creature._abilities["cha"] = 11
    assert creature.ability_modifier("cha") == 0
    creature._abilities["cha"] = 12
    assert creature.ability_modifier("cha") == 1
    creature._abilities["cha"] = 20
    assert creature.ability_modifier("cha") == 5

    creature._abilities["cha"] = 9
    assert creature.ability_modifier("cha") == -1
    creature._abilities["cha"] = 8
    assert creature.ability_modifier("cha") == -1
    creature._abilities["cha"] = 0
    assert creature.ability_modifier("cha") == -5


def test_ability_check(creature: CreatureChild) -> None:

    creature._abilities["cha"] = 10

    for _ in range(N):
        check = creature.ability_check("cha")
        assert check >= 1 and check <= 20


def test_saving_throw(creature: CreatureChild) -> None:

    creature._abilities["cha"] = 10

    for _ in range(N):
        throw = creature.saving_throw("cha")
        assert throw >= 1 and throw <= 20

    creature.add_proficiency("saving-throw-cha")
    for _ in range(N):
        throw = creature.saving_throw("cha")
        assert throw >= 3 and throw <= 22

import pytest
from src.character import Character, client
from src import dnd

N = 100

@pytest.fixture
def character() -> Character:

    client.dry = True
    return Character("test")


@pytest.fixture
def character_init(character: Character) -> Character:

    character.init()
    return character


def test_init(character: Character) -> None:

    # __init__
    assert character._name.startswith("Test")
    assert len(character._race) > 0
    assert len(character._class) > 0

    assert character._level == 1
    assert character._xp == 0
    assert character._hit_dice == character._level

    assert character._proficiency_bonus == 2

    # init
    character.init()

    for ability in character._abilities:
        assert character._abilities[ability] >= 3
        assert character._abilities[ability] <= 20

    assert len(character._proficiencies) > 0
    assert len(character._weapon) > 0
    assert character._armor == {} or len(character._armor) > 0
    assert character._gp >= 40 and character._gp <= 160

    assert character._max_hitpoints > 0
    assert character._hitpoints == character._max_hitpoints


def test_proficient_weapons(character_init: Character) -> None:

    weapons = character_init.proficient_weapons()
    assert len(weapons) > 0


def test_proficient_armor(character_init: Character) -> None:

    armor = character_init.proficient_armor()
    assert armor == [] or len(armor) > 0


def test_armor_class(character_init: Character) -> None:

    ac = character_init.armor_class({})
    assert ac == 10 + character_init.ability_modifier("dex")

    ac = character_init.armor_class(dnd.get_equipment("leather-armor"))
    assert ac == 11 + character_init.ability_modifier("dex")

    ac = character_init.armor_class(dnd.get_equipment("chain-mail"))
    assert ac == 16


def test_weapon_modifier(character_init: Character) -> None:

    modifier = character_init.weapon_modifier()
    assert modifier == character_init.ability_modifier("dex") or modifier == character_init.ability_modifier("str")


def test_spend(character_init: Character) -> None:

    gold = character_init._gp
    assert character_init.spend(1) == gold - 1
    assert character_init._gp == gold - 1


def test_earn(character_init: Character) -> None:

    gold = character_init._gp
    assert character_init.earn(1) == gold + 1
    assert character_init._gp == gold + 1


def test_equip(character_init: Character) -> None:

    equipment = dnd.get_equipment("club")
    assert character_init.equip(equipment)
    assert character_init._weapon == equipment

    equipment = dnd.get_equipment("leather-armor")
    assert character_init.equip(equipment)
    assert character_init._armor == equipment

    equipment = dnd.get_equipment("abacus")
    assert not character_init.equip(equipment)


def test_attack(character_init: Character) -> None:

    attack = character_init.attack(character_init)
    if (attack.attack_type.value <= 0):
        assert attack.damage == 0
    else:
        assert attack.damage > 0


def test_run(character_init: Character) -> None:

    assert character_init.run(character_init._race["speed"])


def test_death_saving_throws(character_init: Character) -> None:

    throws = character_init.death_saving_throws()
    if (throws):
        assert character_init._hitpoints == 1


def test_add_experience(character_init: Character) -> None:

    level = character_init._level
    xp = character_init._xp

    assert not character_init.add_experience(1)
    assert character_init._xp == xp + 1
    assert character_init._level == level

    assert character_init.add_experience(9999)
    assert character_init._level > level


def test_short_rest(character_init: Character) -> None:

    character_init.damage(1)
    hitpoints = character_init._hitpoints
    hitdice = character_init._hit_dice
    rest = character_init.short_rest()
    assert rest > 0
    assert character_init._hit_dice == hitdice - 1
    assert character_init._hitpoints == min(character_init._max_hitpoints, hitpoints + rest)


def test_long_rest(character_init: Character) -> None:

    hitdice = character_init._hit_dice
    character_init.damage(1)
    rest = character_init.long_rest()
    assert rest > 0
    assert character_init._hitpoints == character_init._max_hitpoints
    assert character_init._hit_dice >= hitdice


def test_title(character_init: Character) -> None:

    assert len(character_init.title()) > 0


def test_bio(character_init: Character) -> None:

    assert len(character_init.bio()) > 0


def test_short_bio(character_init: Character) -> None:

    assert len(character_init.short_bio()) > 0


def test_characters() -> None:

    for i in range(N):

        character = Character(f"test{i}")
        test_init(character)
        test_proficient_weapons(character)
        test_proficient_armor(character)
        test_armor_class(character)
        test_spend(character)
        test_earn(character)
        test_equip(character)
        test_attack(character)
        test_run(character)
        test_add_experience(character)
        test_short_rest(character)
        test_long_rest(character)
        test_death_saving_throws(character)
